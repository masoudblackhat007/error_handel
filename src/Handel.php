<?php

namespace builder;

abstract class Handel
{
    public abstract function setObject(string $key, object $object);
}