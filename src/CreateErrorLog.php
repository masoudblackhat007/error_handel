<?php
namespace  builder;

use Carbon\Carbon;
use Morilog\Jalali\Jalalian;

class CreateErrorLog
{
    private array $errors;

    public function __construct($errors )
    {
        $this->errors = $errors;
    }

    public function createError(){
        header('Content-type: text/plain; charset=utf-8');
        $handelErrorDb = new HandelError($this->errors);
        $errors = (new Director())->build($handelErrorDb);
        date_default_timezone_set('Asia/Tehran');
        $errors['time'] = utf8_encode(Jalalian::fromCarbon(Carbon::now())); // 1391-10-02 00:00:00
        // Path of the log file where the errors should be saved
        $logFile = "./errors.log";
        // Saving the error message in the log file
        error_log(json_encode($errors), 3, $logFile);
    }

}