<?php

namespace builder;

class Director
{
    public function build(Check $builder){
        return $builder->createError()->addCode()->addMessage()->getError();
    }
}