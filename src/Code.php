<?php

namespace builder;

class Code
{
    public string $code = "";

    public function setCode(string $code){
        $this->code = $code;
    }

    public function getCode(){
        return $this->code;
    }
}