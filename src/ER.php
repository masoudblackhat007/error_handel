<?php

namespace builder;

class ER extends Handel
{
    private object $object;
    private array $value = [];
    private array $input;

    public function setObject(string $key, object $object){
        // TODO: Implement setObject() method.
        $this->object = $object;
        switch ($object){
            case method_exists($object,"setCode") :
                $object->setCode($key);
                $this->input['code'] = $object->getCode();
                break;
            case method_exists($object,"setMessage") :
                $object->setMessage($key);
                $this->input['message'] = $object->getMessage();
                break;
            case method_exists($object,"setTrace") :
                $object->setTrace($key);
                $this->input['trace'] =  $object->getTrace();
                break;
        }

    }

    public function getObject(){
        return $this->input;
    }
}