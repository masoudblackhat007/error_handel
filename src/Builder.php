<?php

namespace builder;

interface Builder
{
    public function addCode();

    public function addMessage();

    public function addTrace();

    public function createError();

    public function getError();
}