<?php

namespace builder;

abstract class  Check implements Builder
{
    private  $errors;
    private ER $DB;

    public function __construct($errors )
    {
        $this->errors = $errors;
    }

   public function createError(){
       // TODO: Implement createError() method.
       $this->DB = new ER();
       return $this;

   }

   public function addCode(){
       // TODO: Implement addCode() method.
       foreach ($this->errors as $key=>$error) if ($key === "code") $this->DB->setObject($error, new Code());
       return $this;
   }

   public function addMessage()
   {
       // TODO: Implement addMessage() method.
       foreach ($this->errors as $key=>$error) if ($key === "message") $this->DB->setObject($error, new Message());
       return $this;

   }

   public function addTrace(){

       // TODO: Implement addTrace() method.
        foreach ($this->errors as $key=>$error) if ($key === "trace") $this->DB->setObject($error, new Trace());
        return $this;

   }

    public function getError(){
        // TODO: Implement getError() method.
        return $this->DB->getObject();
    }
}