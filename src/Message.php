<?php

namespace builder;
class Message
{

    public string $message = "";

    public function setMessage(string $message){
        $this->message = $message;
    }

    public function getMessage(){
        return $this->message;
    }

}