<?php

namespace builder;

class Trace
{
    public string $trace = "";

    public function setTrace(string $trace){
        $this->trace = $trace;
    }

    public function getTrace(){
        return $this->trace;
    }
}